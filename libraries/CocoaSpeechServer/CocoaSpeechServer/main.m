//
//  main.m
//  CocoaSpeechServer
//
//  Created by Evan Pierzina on 4/1/15.
//  Copyright (c) 2015 Evan Pierzina. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
