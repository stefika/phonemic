#import "AppDelegate.h"
#import "AsyncSocket.h"

#define WELCOME_MSG  0
#define ECHO_MSG     1
#define WARNING_MSG  2

#define READ_TIMEOUT -1             //no timeout
#define READ_TIMEOUT_EXTENSION -1   //no timeout

@interface AppDelegate (PrivateAPI) <NSSpeechSynthesizerDelegate>
@end

@implementation AppDelegate

- (id)init
{
    if((self = [super init]))
    {
        listenSocket = [[AsyncSocket alloc] initWithDelegate:self];
        connectedSockets = [[NSMutableArray alloc] initWithCapacity:1];
        blockingClients = [[NSMutableArray alloc] init];
        
        isRunning = NO;
        isSpeechEnabled = YES;  //This dictates whether speech is enabled by default or not
        clientIsBlocking = NO;
        clientCount = 0;
        lastSpokenMessage = [[NSMutableArray alloc] init];
        
        synth = [[NSSpeechSynthesizer alloc] init]; //start with default voice
        messageQueue = [[NSMutableArray alloc] init];
        [synth setDelegate:self];
        
        [listenSocket setRunLoopModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
        
        //Start the server for phonemic
        int port = 56103;
        
        NSError *error = nil;
        if(![listenSocket acceptOnPort:port error:&error])
            NSLog(@"Error starting server: %@", error);
        else
            NSLog(@"Echo server started on port %hu", [listenSocket localPort]);
    }
    return self;
}

- (void)onSocket:(AsyncSocket *)sock didAcceptNewSocket:(AsyncSocket *)newSocket
{
    [connectedSockets addObject:newSocket];
}

- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    clientCount++;
    NSLog(@"Accepted client %@:%hu", host, port);
    
    NSString *welcomeMsg = @"OK\r\n";
    NSData *welcomeData = [welcomeMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    [sock writeData:welcomeData withTimeout:-1 tag:WELCOME_MSG];
    
    [sock readDataToData:[AsyncSocket CRLFData] withTimeout:READ_TIMEOUT tag:0];
}

- (void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    if(tag == ECHO_MSG)
    {
        [sock readDataToData:[AsyncSocket CRLFData] withTimeout:READ_TIMEOUT tag:0];
    }
}

- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    NSData *strData = [data subdataWithRange:NSMakeRange(0, [data length] - 2)];
    NSString *msg = [[NSString alloc] initWithData:strData encoding:NSUTF8StringEncoding];
    
    NSArray* parsedMessage = [msg componentsSeparatedByString:@":"]; //parse the phonemic string (ex: speak:MEDIUM:TEXT:blah)
    
    //concatenate any parts of the message after the first three ':' separations
    if (parsedMessage.count > 4) {
        NSString *message = parsedMessage[3];
        for (NSUInteger i = 4; i < parsedMessage.count; i++) {
            message = [NSString stringWithFormat:@"%@:%@", message, parsedMessage[i]];
        }
        parsedMessage = [NSArray arrayWithObjects:parsedMessage[0], parsedMessage[1], parsedMessage[2], message, nil];
    }
    
    //Decode the message and perform the necassary actions
    //Note: the server must write before reading again, hence the dummy messages below when no response is necessary
    void(^selectedCase)() = @{
                              @"canBlock" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"canPause" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"canResume" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"canSetPitch" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"canSetSpeed" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"canSetVoice" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"canSetVolume" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"canStop" : ^{
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"getAvailableEngines" : ^{
                                  NSString *availableEngines = @"APPLE_COCOA\r\n";
                                  [self returnMessage:availableEngines toSocket:sock];
                                  [self returnMessage:@"\r\n" toSocket:sock]; //client awaits a blank line in order to stop
                              },
                              @"getAvailableVoices" : ^{
                                  NSArray *voices = [[NSArray alloc]initWithArray:[NSSpeechSynthesizer availableVoices]]; //strings
                                  for (NSUInteger i = 0; i < voices.count; i++) {
                                      //trim the first 33 characters "com.apple.speech.synthesis.voice." and append \r\n
                                      [self returnMessage:[NSString stringWithFormat:@"%@%@",[voices[i] substringFromIndex:33], @"\r\n"] toSocket:sock];
                                  }
                                  [self returnMessage:@"\r\n" toSocket:sock]; //client awaits a blank line in order to stop
                              },
                              @"getCurrentVoice" : ^{
                                  NSString *currentVoice = [NSString stringWithFormat:@"%@%@", [synth voice], @"\r\n"];
                                  [self returnMessage:currentVoice toSocket:sock];
                              },
                              @"getPitch" : ^{
                                  //TODO
                                  //                                  id test = [synth objectForProperty:@"NSSpeechCurrentVoiceProperty" error:NULL];
                                  
                                  NSString *pitch = @"1.0\r\n"; //This won't change currently
                                  [self returnMessage:pitch toSocket:sock];
                              },
                              @"getSpeed" : ^{
                                  float speed = ([synth rate] - 100) / 200; //100-300wpm
                                  NSString *rate = [NSString stringWithFormat:@"%f%@", speed, @"\r\n"];
                                  [self returnMessage:rate toSocket:sock];
                              },
                              @"getTextToSpeechEngine" : ^{
                                  NSString *ttsEngine = @"APPLE_COCOA\r\n";
                                  [self returnMessage:ttsEngine toSocket:sock];
                              },
                              @"getVersion" : ^{
                                  NSString *version = @"2.0\r\n"; //Phonemic version
                                  [self returnMessage:version toSocket:sock];
                              },
                              @"getVolume" : ^{
                                  NSString *volume = [NSString stringWithFormat:@"%f\r\n", [synth volume]];
                                  [self returnMessage:volume toSocket:sock];
                              },
                              @"isSpeaking" : ^{
                                  [self sendBoolMessage:synth.isSpeaking toSocket:sock];
                              },
                              @"isSpeechEnabled" : ^{
                                  [self sendBoolMessage:isSpeechEnabled toSocket:sock];
                              },
                              @"pause" : ^{
                                  if ([synth isSpeaking]) {
                                      [synth pauseSpeakingAtBoundary:NSSpeechImmediateBoundary];
                                      [self sendBoolMessage:true toSocket:sock];
                                  }
                              },
                              @"reinitialize" : ^{
                                  [self returnMessage:@"dummyMessage\r\n" toSocket:sock];
                              },
                              @"respeak" : ^{
                                  if (isSpeechEnabled && lastSpokenMessage.count > 0) {
                                      [self enqueueMessage:lastSpokenMessage];
                                      [self returnMessage:@"dummyMessage\r\n" toSocket:sock];
                                  }
                                  else
                                      [self sendBoolMessage:true toSocket:sock];
                              },
                              @"resume" : ^{
                                  [synth continueSpeaking];
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"setPitch" : ^{
                                  //TODO
                                  [self sendBoolMessage:true toSocket:sock]; //currently not implemented
                              },
                              @"setSpeechEnabled" : ^{
                                  isSpeechEnabled = [parsedMessage[1] boolValue];
                                  [self returnMessage:@"dummyMessage\r\n" toSocket:sock];
                              },
                              @"setSpeed" : ^{
                                  //NSSPeechSynthesizer documentation suggests human speech is 180-220 wpm
                                  float speed = 100 + 200 * [parsedMessage[1] floatValue]; //100 - 300 wpm
                                  [synth setRate:speed]; //Note: this is an asynchronous call, so changes may not be noticed immediately
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"setTextToSpeechEngine" : ^{
                                  [self sendBoolMessage:true toSocket:sock]; //Mac users do not have speech engine options
                              },
                              @"setVoice" : ^{
                                  [self sendBoolMessage:[synth setVoice:[NSString stringWithFormat:@"%@%@", @"com.apple.speech.synthesis.voice.", parsedMessage[1]]] toSocket:sock];
                              },
                              @"setVolume" : ^{
                                  [synth setVolume:[parsedMessage[1] floatValue]];
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              @"speak" : ^{
                                  if (isSpeechEnabled)
                                      [self enqueueMessage:parsedMessage];
                                  [self returnMessage:@"dummyMessage\r\n" toSocket:sock];
                              },
                              @"speakBlocking" : ^{
                                  if (isSpeechEnabled) {
                                      [blockingClients addObject:sock];
                                      [self enqueueMessage:parsedMessage];
                                  }
                                  else
                                      [self sendBoolMessage:true toSocket:sock];
                              },
                              @"stop" : ^{
                                  if ([synth isSpeaking])
                                      [synth stopSpeaking];
                                  [self sendBoolMessage:true toSocket:sock];
                              },
                              }[parsedMessage[0]];
    if (selectedCase != nil)
        selectedCase();
    
//        if(msg)
//            NSLog(@"%@", msg);
    //    else
    //        NSLog(@"Error converting received data into UTF-8 String");
}

- (void)returnMessage:(NSString *)message toSocket:(AsyncSocket *)sock {
    NSData *returnData = [message dataUsingEncoding:NSUTF8StringEncoding];
    [sock writeData:returnData withTimeout:-1 tag: 1];
}

- (void)sendBoolMessage:(BOOL)value toSocket:(AsyncSocket *)sock {
    NSString *returnMsg;
    if (value == true)
        returnMsg = @"true\r\n";
    else
        returnMsg = @"false\r\n";
    
    NSData *returnData = [returnMsg dataUsingEncoding:NSUTF8StringEncoding];
    
    [sock writeData:returnData withTimeout:-1 tag: 1];
    
}

- (void)enqueueMessage:(NSArray*) parsedMessage {
    if ([parsedMessage[3] isNotEqualTo: @""]) {
        //create a Message object with a priority (the string will be converted to int) and a message
        Message* priorityMessage = [[Message alloc] initWithCommand:parsedMessage[0] priority:parsedMessage[1] message:parsedMessage[3]];
        
        //check to see if there are any current messages speaking (check the queue just in case as well)
        if (!synth.isSpeaking && messageQueue.count == 0) {
            [messageQueue addObject:priorityMessage];
            [synth startSpeakingString:priorityMessage->message];
            if ([priorityMessage->command isEqual: @"speakBlocking"])
                clientIsBlocking = YES;
            [lastSpokenMessage setArray:parsedMessage];
        }
        
        //reaching this point most likely means something is being spoken currently
        else {
            //check to see if any messages are enqueued at this point
            if (messageQueue.count != 0 && priorityMessage->priority >= ((Message*)[messageQueue objectAtIndex:0])->priority) {
                //something is being spoken, stop it if the current message has a >= prirorty by comparison
                
                [synth stopSpeaking];
                [messageQueue addObject:priorityMessage];
            }
            //nothing in the queue, just place it at the front
            else if (messageQueue.count == 0)
                [messageQueue addObject:priorityMessage];
        }
    }
}

- (void)speechSynthesizer:(NSSpeechSynthesizer *)sender didFinishSpeaking:(BOOL)success {
    //check if this call came from a blocking message
    if (clientIsBlocking) {
        clientIsBlocking = NO;
        //tell the client the blocking message has finished
        NSString *blockMsg = @"true\r\n";
        NSData *blockData = [blockMsg dataUsingEncoding:NSUTF8StringEncoding];
        //write to the client that initiated the blocking call
        if (blockingClients.count) {
            [blockingClients[0] writeData:blockData withTimeout:-1 tag:1];
            [blockingClients removeObjectAtIndex:0];
        }
    }
    //remove the spoken message from the queue
    if (messageQueue.count > 0) //may have already been removed if a higher priority message came in
        [messageQueue removeObjectAtIndex:0];
    //speak the next message if it exists
    if (messageQueue.count > 0) {
        Message *nextMessage = messageQueue[0]; //intermediate step since NSMutableArray doesn't know what types it holds
        [synth startSpeakingString:nextMessage->message];
        if ([nextMessage->command isEqual: @"speakBlocking"])
            clientIsBlocking = YES;
        [lastSpokenMessage replaceObjectAtIndex:0 withObject:nextMessage->command];
        [lastSpokenMessage replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:nextMessage->priority]];
        [lastSpokenMessage replaceObjectAtIndex:3 withObject:nextMessage->message];
    }
}

- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err
{
    NSLog(@"Client Disconnected: %@:%hu", [sock connectedHost], [sock connectedPort]);
}

- (void)onSocketDidDisconnect:(AsyncSocket *)sock
{
    [connectedSockets removeObject:sock];
    clientCount--;
    //exit server if no client is connected
    if (clientCount < 1) {
        exit(0);
    }
}

@end