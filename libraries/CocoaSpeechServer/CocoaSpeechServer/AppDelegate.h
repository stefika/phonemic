#import <Cocoa/Cocoa.h>
#import "Message.h"

@class AsyncSocket;

@interface AppDelegate : NSObject
{
    AsyncSocket *listenSocket;
    NSMutableArray *connectedSockets;
    NSSpeechSynthesizer *synth;
    NSMutableArray *messageQueue;
    NSMutableArray *lastSpokenMessage;
    NSMutableArray *blockingClients;
    
    BOOL isRunning;
    BOOL isSpeechEnabled;
    BOOL clientIsBlocking;
    uint clientCount;
}
- (void)enqueueMessage:(NSArray*) parsedMessage;
- (void)sendBoolMessage:(BOOL) value toSocket:(AsyncSocket *)sock;
- (void)returnMessage:(NSString *)message toSocket:(AsyncSocket *)sock;
@end