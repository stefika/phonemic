//
//  Message.h
//  MacSpeechServer
//
//  Created by Evan Pierzina on 2/21/15.
//
//

#import <Foundation/Foundation.h>

@interface Message : NSObject {
    @public int priority;
    NSString *message;
    NSString *command;
}

- (id)initWithCommand:(NSString*)cmd priority:(NSString*)pr message:(NSString*)msg;

@end
