//
//  Message.m
//  MacSpeechServer
//
//  Created by Evan Pierzina on 2/21/15.
//
//

#import "Message.h"

#define LOWEST      0
#define LOW         1
#define MEDIUM_LOW  2
#define MEDIUM      3
#define MEDIUM_HIGH 4
#define HIGH        5
#define HIGHEST     6
#define BLOCKING    7

@implementation Message

- (id)initWithCommand:(NSString*)cmd priority:(NSString*)pr message:(NSString*)msg
{
    if((self = [super init]))
    {
        //change the priority from a string to an integer
        //this is basically a switch statement for strings in Objective-C
        void (^selectedCase)() = @{
                                   @"LOWEST" : ^{ priority = LOWEST; },
                                   @"LOW" : ^{ priority = LOW; },
                                   @"MEDIUM_LOW" : ^{ priority = MEDIUM_LOW; },
                                   @"MEDIUM" : ^{ priority = MEDIUM; },
                                   @"MEDIUM_HIGH" : ^{ priority = MEDIUM_HIGH; },
                                   @"HIGH" : ^{ priority = HIGH; },
                                   @"HIGHEST" : ^{ priority = HIGHEST; },
                                   @"BLOCKING" : ^{ priority = BLOCKING; },
                                   }[pr];
        
        //perform the selected block
        if (selectedCase != nil)
            selectedCase();
        
        message = msg;
        command = cmd;
    }
    return self;
}

@end
